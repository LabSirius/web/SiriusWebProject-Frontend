const express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
require("colors");

const index = require("./routes/index");
const users = require("./routes/users");
const session = require("./routes/session");
const projects = require("./routes/projects");
const news = require("./routes/news");
const events = require("./routes/events");
const wip = require("./routes/wip");

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, "public", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// routes(app, '/');
// users(app, '/users');
// session(app, '/session');
app.use("/", index);
app.use("/projects", projects);
app.use("/users", users);
app.use("/session", session);
app.use("/news", news);
app.use("/events", events);

// projects(app, '/projects');

// catch 404 and forward to error handler
app.use((req, res) => {
  // var err = new Error('Not Found');
  // err.status = 404;
  // next(err);
  res.render("404", { active: {} });
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "dev") {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

module.exports = app;
