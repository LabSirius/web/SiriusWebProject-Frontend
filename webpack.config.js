const debug = process.env.NODE_ENV !== "prod";
const webpack = require("webpack");
const path = require("path");

module.exports = {
  context: path.join(__dirname),
  entry: "./browser/main.js",
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "stage-0"]
        }
      }
    ]
  },
  output: {
    path: __dirname + "/public/js",
    filename: "bundle.js"
  },
  plugins: debug
    ? []
    : [
        new webpack.optimize.UglifyJsPlugin({
          compress: { warnings: false },
          mangle: false,
          sourcemap: false
        })
      ]
};
