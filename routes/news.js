const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.render("news/news", { active: { news: "active" } });
});

router.get("/create", (req, res) => {
  res.render("news/create", { active: { create: "active" } });
});

router.get("/edit", (req, res) => {
  res.render("news/edit", { active: { edit: "active" } });
});

module.exports = router;
