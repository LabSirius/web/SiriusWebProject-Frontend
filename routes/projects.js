const express = require("express");
const router = express.Router();

const projects = [
  {
    title: "Análisis de accesibilidad en Pereira y Dosquebradas usando velocidades operativas",
    summary: `Este trabajo hace un análisis de accesibilidad a las ciudades courbanizadas de Pereira y Dosquebradas usando modelos de accesibilidad geográfica y de oportunidades acumuladas. El modelo de accesibilidad geográfico toma los tiempos promedios de viaje bien sea entre todos los puntos de la red de transporte o de todos los puntos a  un grupo particular de nodos de actividad (oportunidades) y hace una interpolación espacial para construir curvas de nivel asociados con niveles de accesibilidad. Por su parte el modelo de oportunidades acumuladas calcula, para cada zona del área de estudio y para un tiempo máximo, la cantidad de oportunidades que alcanza cada una de las zonas dentro de ese tiempo. Ambos modelos permiten visualizar los resultados en mapas facilitando la interpretación de los resultados, los análisis  y la comparaciones de diferentes escenarios.

    Los resultados permiten medir la eficiencia de la infraestructura de transporte e identificar áreas con bajos niveles de accesibilidad. En este sentido se encuentra que Pereira como ciudad monocéntrica tiene gran parte de sus nodos de actividad en el centro. Adicionalmente los tiempos de viaje son menores en esta zona y se van incrementando hacia la periferia. Llama la atención que se encuentran zonas con muy altos tiempos de viaje lo que sugiere que se están empezando a generar areas en condiciones de exclusión social, es el caso de la Comuna Villa Santana, de la Parte Sur del Barrio Cuba, del Barrio Parque Industrial y del sector de Frailes en Dosquebradas.`,
    img: "proyecto1.png",
    authors: ["Orlando Antonio Sabogal Cardona"],
    date: "Sep/2016 - Mar/2017"
  },
  {
    title: "Accessibility patterns and equity, spatio-temporal characteristics explorations",
    summary: `Accessibility has become a key concept in transport analysis and urban planning with recent feedbacks from the fields of social exclusion and social justice, there is an interest in understanding inequalities associated to the lack of transportation. Also, accessibility has proved to be a useful tool for policy making processes in the so called developed world and can be adopted and adapted for applications in the global south. Spatial characteristics of accessibility and its distribution among income groups are regularly considered and, despite a wide range of approaches and interpretations, it can be defined as the easiness or difficulty associated to reach essential opportunities such as employment, education or health care. Although academic literature and practitioners have been award of the temporal dimension of accessibility, most of the studies have focused on the time people expend during the execution of activities and not on the changes in accessibility due to temporal changes in congestion, buses frequency, peak hours effects and specific elements of days, weeks and months. Additionally, transportation analysis and accessibility research can now use information captured through GPS devices and process it and analyzed with geographic information systems. In this research, we gathered speed values with GPS to the midterm co-urbanized area of Pereira and Dosquebradas cities and explore changes in the accessibility territorial model for different hours, and compare results for typical days (Thursday, Wednesday, Tuesday), Mondays, Fridays and weekends as well as for months. Territorial model uses speed values and computes mean travel times for every node in the transportation network to all other nodes or to specific type of opportunities and yield to a map with isochronous lines over the analyzed area. Results indicates that accessibility levels change considerably within a day and that the effects are not distributed in a even fashion and that there are segregated areas in Pereira and Dosquebradas with people living in social exclusion conditions.`,
    img: "proyecto1.png",
    authors: ["Orlando Antonio Sabogal Cardona"],
    date: "02/05/2012"
  },
  {
    title: "Aceleración del Metodo de Diferencias Finitas Para la Simulación del Proceso de Generación del Plasma a través de Ablación Láser",
    img: "proyecto1.png",
    authors: [
      "David Alejandro Jimenez",
      "Orlando Antonio Sabogal Cardona",
      "José Alfredo Jaramillo Villegas"
    ],
    date: "02/05/2012"
  },
  {
    title: "Aceleración del Metodo de Diferencias Finitas Para la Simulación del Proceso de Generación del Plasma a través de Ablación Láser",
    img: "proyecto1.png",
    authors: [
      "David Alejandro Jimenez",
      "Orlando Antonio Sabogal Cardona",
      "José Alfredo Jaramillo Villegas"
    ],
    date: "02/05/2012"
  },
  {
    title: "Aceleración del Metodo de Diferencias Finitas Para la Simulación del Proceso de Generación del Plasma a través de Ablación Láser",
    img: "proyecto1.png",
    authors: [
      "David Alejandro Jimenez",
      "Orlando Antonio Sabogal Cardona",
      "José Alfredo Jaramillo Villegas"
    ],
    date: "02/05/2012"
  },
  {
    title: "Aceleración del Metodo de Diferencias Finitas Para la Simulación del Proceso de Generación del Plasma a través de Ablación Láser",
    img: "proyecto1.png",
    authors: [
      "David Alejandro Jimenez",
      "Orlando Antonio Sabogal Cardona",
      "José Alfredo Jaramillo Villegas"
    ],
    date: "02/05/2012"
  },
  {
    title: "IMPACTO EN LAS RUTAS DEL MEGABUS CON LA IMPLEMENTACION DE ESTRATEGIAS TSP Y CAMBIO EN LA CONFIGURACION DE LOS SEMAFOROS",
    summary: `Las estrategias TSP (Transit Signal Priority) buscan darle prioridad en las intersecciones semaforizadas al transporte publico, y mediante la construccion de modelos y mirosimulacion se cuantifico el impacto que se puede tener en las ciudades de Pereira y Dosquebradas si se implementa un sistema de prioridad en la red de los buses articulados de MEGABUS.
    A partir de este proyecto se nota la evidencia de buscar estrategias TSP innovadoras que no solo permitan ahorrar en costos de implementacion y cuidado ambiental; sino que tambien incluya conceptos de trafico adaptativo, actuado y dinamico.`,
    img: "proyecto1.png",
    authors: ["Orlando Antonio Sabogal Cardona"],
    date: "02/05/2012"
  }
];

router.get("/", (req, res) => {
  res.render("projects/projects", {
    projects: projects,
    active: { projects: "active" }
  });
});

router.get("/project", (req, res) => {
  res.render("projects/project");
});

router.get("/new", (req, res) => {
  res.render("projects/new", { active: {} });
});

module.exports = router;
// (app, mountPoint) => {
//
//   app.use(mountPoint, router);
// };
