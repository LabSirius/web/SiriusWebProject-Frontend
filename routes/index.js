const express = require("express"),
  router = express.Router(),
  request = require("request");

/* GET home page. */
router.get("/", (req, res) => {
  res.render("index", { active: { index: "active" } });
});

router.get("/about", (req, res) => {
  res.render("about", { active: { about: "active" } });
});

router.get("/hpc", (req, res) => {
  res.render("hpc", { active: { hpc: "active" } });
});

router.get("/its", (req, res) => {
  res.render("its", { active: { its: "active" } });
});

router.post("/contact", (req, res) => {
  console.log(req.body["g-recaptcha-response"]);

  if (
    req.body["g-recaptcha-response"] !== undefined ||
    req.body["g-recaptcha-response"] !== "" ||
    req.body["g-recaptcha-response"] !== null
  ) {
    const secretKey = "";
    // req.connection.remoteAddress will provide IP address of connected user.
    const verificationUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body["g-recaptcha-response"]}&remoteip=${req.connection.remoteAddress}`;
    console.log(verificationUrl);
    // Hitting GET request to the URL, Google will respond with success or error scenario.
    request(verificationUrl, (error, response, body) => {
      body = JSON.parse(body);
      if (body.success !== undefined && !body.success)
        console.log("FAILED");
      else {
        const { name, email, message } = req.body;
        const transporter = nodemailer.createTransport(
          `smtps://${auth.user}:${auth.pass}@smtp.gmail.com`
        );
        const mailOptions = {
          from: `Sirius <info@sirius.utp.edu.co>`,
          to: "info@sirius.utp.edu.co",
          subject: "Contacto::Web Sirius",
          html: `<p>Correo desde Contacto | Web Sirius</ p><br />
              <b>Nombre: </b>${name}<br />
              <b>Correo Electrónico: </b>${email}<br />
              <hr />
              <p>${message}</p>`
        };

        transporter.sendMail(mailOptions, (err, res) => {
          if (err) console.log(err);
          else console.log(res);
        });
      }
    });
  }
  res.redirect("/");
});

router.get("/12ccc", (req, res) => {
  res.redirect("./Afiche12CCC.pdf");
});

module.exports = router;
// module.exports = (app, mountPoint) => {
//
//   app.use(mountPoint, router);
// };
