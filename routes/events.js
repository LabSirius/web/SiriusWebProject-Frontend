const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.render("events/calendar", { active: { events: "active" } });
});

router.get("/create", (req, res) => {
  res.render("events/create", { active: { create: "active" } });
});

router.get("/:id/show", (req, res) => {
  res.render("events/show", { active: { show: "active" } });
});

router.get("/:id", (req, res) => {
  res.render("events/edit", { active: { edit: "active" } });
});

module.exports = router;
