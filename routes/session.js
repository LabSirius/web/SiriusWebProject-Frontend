const express = require("express");
const router = express.Router();

router.get("/login", (req, res) => {
  res.render("session/login", { active: { index: "" } });
});

module.exports = router;
