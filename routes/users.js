const express = require("express");
const router = express.Router();

/* GET users listing. */
router.get("/", (req, res) => {
  res.render("users/users", { active: { users: "active" } });
});

router.get("/register", (req, res) => {
  res.render("users/register", { active: {} });
});

router.get("/admin", (req, res) => {
  res.render("users/admin", { active: {} });
});

router.get("/emailRecovery", (req, res) => {
  res.render("users/newPassword", { active: {} });
});

router.get("/reset/:token", (req, res) => {
  res.render("users/resetPassword", { active: {} });
});

router.get("/profile", (req, res) => {
  res.render("users/profile", { active: {} });
});

router.get("/pending", (req, res) => {
  res.render("users/pending", { active: {} });
});

router.get("/:username", (req, res) => {
  res.render("users/username", { active: {} });
});

module.exports = router;
