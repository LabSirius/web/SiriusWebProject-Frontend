$(document).ready(function() {
  // fix menu when passed
  $(".masthead").visibility({
    once: false,
    onBottomPassed: function() {
      $(".fixed.menu").transition("fade in");
    },
    onBottomPassedReverse: function() {
      $(".fixed.menu").transition("fade out");
    }
  });
  $("input:text, .ui.button", ".ui.action.input").on("click", function(e) {
    $("input:file", $(e.target).parents()).click();
  });

  $("input:file", ".ui.action.input").on("change", function(e) {
    var name = e.target.files[0].name;
    $("input:text", $(e.target).parent()).val(name);
  });

  // create sidebar and attach to menu open
  $(".ui.sidebar").sidebar("attach events", ".toc.item");
});
