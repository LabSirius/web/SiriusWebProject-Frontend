# SiriusWebProject-Frontend

[![Build Status](https://travis-ci.org/labsirius/SiriusWebProject-Frontend.svg?branch=master)](https://travis-ci.org/labsirius/SiriusWebProject-Frontend)
[![dependencies Status](https://david-dm.org/labsirius/SiriusWebProject-Frontend/status.svg)](https://david-dm.org/labsirius/SiriusWebProject-Frontend)
[![Github Issues](https://img.shields.io/github/issues/labsirius/SiriusWebProject-Frontend.svg)](http://github.com/labsirius/SiriusWebProject-Frontend/issues)

## Requirements/Dependencies

- [NodeJS](https://nodejs.org/en/)
- [Polymer](https://www.polymer-project.org/1.0/)
- [Bower](https://bower.io/)

### Others

- [Flexbox Grid](https://github.com/kristoferjoseph/flexboxgrid)
- Semantic UI

## Installation

### Node Dependencies:
```
(sudo) npm install
```

### Semantic UI

```
npm install -g gulp
cd public/semantic/
gulp build
```


## Run

```
npm start or yarn start
npm run prod or yarn prod
```
___
<!-- <p align="center"><img src="http://forthebadge.com/images/badges/made-with-crayons.svg"></img></p> -->
<p align="center"><img src="http://forthebadge.com/images/badges/built-by-developers.svg"></img></p>
<a href="http://sirius.utp.edu.co" target="_blank"><p align="center"><img src="https://github.com/jointDeveloper/web/blob/master/public/IMG/icons/sirius.png?raw=true" width=110px></img></p></a>
