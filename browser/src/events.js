const utils = require("../utils");

module.exports = (page, mountPoint) => {
  page(mountPoint, ctx => {
    utils.getResourceFromServer(backendAddress, mountPoint, (err, res) => {
      if (err || !res.body.success) {
        document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para mostrar eventos
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
      } else {
        let events = res.body.data;
        events = events.map(event => {
          return {
            id: event._id,
            title: event.name,
            start: new Date(event.startDate),
            end: new Date(event.finishDate)
          };
        });

        $("#calendar").fullCalendar({
          header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay,listWeek"
          },
          navLinks: true, // can click day/week names to navigate views
          editable: true,
          eventLimit: true, // allow "more" link when too many events
          events: events,
          eventClick(calEvent) {
            if (window.localStorage.username) {
              window.location.href = `/events/${calEvent.id}`;
            } else {
              window.location.href = `/events/${calEvent.id}/show`;
            }
          }
        });
      }
    });
  });

  page(`${mountPoint}/create`, ctx => {
    if (utils.isAuthorized()) {
      utils.getResourceFromServer(backendAddress, "/users", (err, res) => {
        if (err || !res.body.success) {
          document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para mostrar usuarios
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
        } else {
          const btnEvent = document.getElementById("btn_event");
          let users = res.body.data;
          users = users.map(user => {
            let photo = "/img/user.png";
            if (user.photo) photo = user.photo;
            return `
            <div class="item" data-value="${user.username}">
              <img class="ui avatar image" src="${photo}">
                ${user.firstName} ${user.lastName}
              </div>`;
          });

          users = users.join("\n");
          document.getElementById("authors").innerHTML = users;
          btnEvent.addEventListener("click", ev => {
            ev.preventDefault();
            const file = document.getElementById("photo").files[0];

            if (file) {
              const data = new FormData();
              data.append("photo", file);
              data.append(
                "filename",
                (Date.now() + document.getElementById("name").value).replace(
                  /\s/g,
                  ""
                )
              );
              utils.postToServer(storageAddress, "/img", data, (err, res) => {
                if (err || !res.body.success) {
                  document.getElementById("msg").innerHTML = `
                    <div class="ui negative message center aligned container">
                      <i class="close icon"></i>
                      <div class="header">
                        Problemas guardando la imagen
                      </div>
                      <p>Intenta nuevamente</p>
                    </div>`;
                } else {
                  let warnings = [];
                  if (!document.getElementById("name").value)
                    warnings.push("<li>* Nombre</li>");
                  if (!document.getElementById("startDate").value)
                    warnings.push("<li>* Fecha de Inicio</li>");
                  if (!document.getElementById("finishDate").value)
                    warnings.push("<li>* Fecha de Finalización</li>");
                  if (!document.getElementById("place").value)
                    warnings.push("<li>* Sitio</li>");
                  if (!document.getElementById("registered").value)
                    warnings.push("<li>* Autores</li>");
                  if (warnings.length > 0) {
                    document.getElementById("msg").innerHTML = `
                      <div class="ui negative message center aligned container">
                        <i class="close icon"></i>
                        <div class="header">
                          Los siguientes campos son requeridos:
                        </div>
                        <ul>${warnings.join("")}</li>
                      </div>`;
                  } else {
                    const event = {
                      name: document.getElementById("name").value,
                      startDate: new Date(
                        document.getElementById("startDate").value
                      ),
                      finishDate: new Date(
                        document.getElementById("finishDate").value
                      ),
                      author: localStorage.username,
                      description: document.getElementById("description").value,
                      createdBy: {
                        registered: document
                          .getElementById("registered")
                          .value.split(","),
                        others: []
                      },
                      img: res.body.data,
                      tags: document.getElementById("tags").value.split(","),
                      place: document.getElementById("place").value,
                      resources: document
                        .getElementById("resources")
                        .value.split(",")
                    };

                    utils.postSecuredData(
                      backendAddress,
                      mountPoint,
                      event,
                      (err, res) => {
                        if (err || !res.body.success) {
                          document.getElementById("msg").innerHTML = `
                            <div class="ui negative message center aligned container">
                              <i class="close icon"></i>
                              <div class="header">
                                Problemas para crear evento
                              </div>
                              <p>Verifica que los datos sean los correctos e intenta nuevamente</p>
                            </div>`;
                        } else
                          window.location.href = `/events/${res.body.data._id}`;
                      }
                    );
                  }
                }
              });
            } else {
              let warnings = [];
              if (!document.getElementById("name").value)
                warnings.push("<li>* Nombre</li>");
              if (!document.getElementById("startDate").value)
                warnings.push("<li>* Fecha de Inicio</li>");
              if (!document.getElementById("finishDate").value)
                warnings.push("<li>* Fecha de Finalización</li>");
              if (!document.getElementById("place").value)
                warnings.push("<li>* Sitio</li>");
              if (!document.getElementById("registered").value)
                warnings.push("<li>* Autores</li>");
              if (warnings.length > 0) {
                document.getElementById("msg").innerHTML = `
                  <div class="ui negative message center aligned container">
                    <i class="close icon"></i>
                    <div class="header">
                      Los siguientes campos son requeridos:
                    </div>
                    <ul>${warnings.join("")}</li>
                  </div>`;
              } else {
                const event = {
                  name: document.getElementById("name").value,
                  startDate: new Date(
                    document.getElementById("startDate").value
                  ),
                  finishDate: new Date(
                    document.getElementById("finishDate").value
                  ),
                  author: localStorage.username,
                  description: document.getElementById("description").value,
                  createdBy: {
                    registered: document
                      .getElementById("registered")
                      .value.split(","),
                    others: []
                  },
                  tags: document.getElementById("tags").value.split(","),
                  place: document.getElementById("place").value,
                  resources: document
                    .getElementById("resources")
                    .value.split(",")
                };
                utils.postSecuredData(
                  backendAddress,
                  mountPoint,
                  event,
                  (err, res) => {
                    if (err || !res.body.success) {
                      document.getElementById("msg").innerHTML = `
                      <div class="ui negative message center aligned container">
                        <i class="close icon"></i>
                        <div class="header">
                          Problemas para crear evento
                        </div>
                        <p>Verifica que los datos sean los correctos e intenta nuevamente</p>
                      </div>`;
                    } else
                      window.location.href = `/events/${res.body.data._id}`;
                  }
                );
              }
            }
          });
        }
      });
    }
  });

  page(`${mountPoint}/:id/show`, ctx => {
    const { id } = ctx.params;

    utils.getResourceFromServer(
      backendAddress,
      `${mountPoint}/${id}`,
      (err, res) => {
        if (err || !res.body.success) {
          document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para mostrar evento
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
        } else {
          const event = res.body.data;
          let photo = event.img || "/img/user.png";

          document.getElementById("name").value = event.name;
          document.getElementById("startDate").value = new Date(
            event.startDate
          );
          document.getElementById("finishDate").value = new Date(
            event.finishDate
          );
          document.getElementById(
            "registered"
          ).innerText = event.createdBy.registered.join(", ");
          document.getElementById("tags").innerText = event.tags.join(", ");
          document.getElementById("place").value = event.place;
          document.getElementById("resources").innerText = event.resources.join(
            ", "
          );
          document.getElementById("description").value = event.description;
          document.getElementById("photo").src = photo;
        }
      }
    );
  });

  page(`${mountPoint}/:id`, ctx => {
    const { id } = ctx.params;
    const btnEvent = document.getElementById("btn_event");
    const btnEdit = document.getElementById("eventEdit");
    const btnDelete = document.getElementById("delete");
    const inputs = document.getElementsByClassName("disabled");

    for (let i = 0; i < inputs.length; i++) {
      inputs[i].disabled = true;
    }

    btnEdit.addEventListener("click", ev => {
      let flag = true;
      for (let i = 0; i < inputs.length; i++) {
        inputs[i].disabled = !inputs[i].disabled;
        flag = inputs[i].disabled;
      }
      if (!flag) {
        btnEvent.style.display = "block";
      } else {
        btnEvent.style.display = "none";
      }
    });

    if (utils.isAuthorized()) {
      utils.getResourceFromServer(backendAddress, "/users", (err, res) => {
        if (err || !res.body.success) {
          document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para mostrar usuarios
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
        } else {
          let users = res.body.data;
          users = users.map(user => {
            let photo = "/img/user.png";
            if (user.photo) photo = user.photo;
            return `
            <div class="item" data-value="${user.username}">
              <img class="ui avatar image" src="${photo}">
                ${user.firstName} ${user.lastName}
              </div>`;
          });

          users = users.join("\n");
          document.getElementById("authors").innerHTML = users;
          utils.getResourceFromServer(
            backendAddress,
            `${mountPoint}/${id}`,
            (err, res) => {
              if (err || !res.body.success) {
                document.getElementById("msg").innerHTML = `
                <div class="ui negative message center aligned container">
                  <i class="close icon"></i>
                  <div class="header">
                    Problemas para mostrar evento
                  </div>
                  <p>Intenta nuevamente</p>
                </div>`;
              } else {
                const event = res.body.data;
                let eventId = event._id;
                let filename = Date.now() + event.name;
                if (event.img) {
                  filename = event.img.split("/img/")[1].split(".")[0];
                }
                if (event.author === localStorage.username) {
                  btnDelete.addEventListener("click", ev => {
                    if (
                      confirm("¿Esta seguro que desea eliminar este evento?")
                    ) {
                      if (event.img) {
                        const filename = event.img.split("/img/")[1];
                        utils.deleteResourceFromServer(
                          storageAddress,
                          `/img/${filename}`,
                          err => {
                            if (err || !res.body.success) {
                              document.getElementById("msg").innerHTML = `
                              <div class="ui negative message center aligned container">
                                <i class="close icon"></i>
                                <div class="header">
                                  Problemas borrando la imagen
                                </div>
                                <p>Intenta nuevamente</p>
                              </div>`;
                            } else {
                              utils.delSecuredResource(
                                backendAddress,
                                `${mountPoint}/${eventId}`,
                                err => {
                                  if (err || !res.body.success) {
                                    document.getElementById("msg").innerHTML = `
                                    <div class="ui negative message center aligned container">
                                      <i class="close icon"></i>
                                      <div class="header">
                                        Problemas borrando el evento
                                      </div>
                                      <p>Intenta nuevamente</p>
                                    </div>`;
                                  } else {
                                    window.location.href = "/events";
                                  }
                                }
                              );
                            }
                          }
                        );
                      } else {
                        utils.delSecuredResource(
                          backendAddress,
                          `${mountPoint}/${eventId}`,
                          err => {
                            if (err || !res.body.success) {
                              document.getElementById("msg").innerHTML = `
                              <div class="ui negative message center aligned container">
                                <i class="close icon"></i>
                                <div class="header">
                                  Problemas borrando el evento
                                </div>
                                <p>Intenta nuevamente</p>
                              </div>`;
                            } else {
                              window.location.href = "/events";
                            }
                          }
                        );
                      }
                    }
                  });

                  let photo = event.img || "/img/user.png";
                  document.getElementById("name").value = event.name;
                  document.getElementById("startDate").value = new Date(
                    event.startDate
                  );
                  document.getElementById("finishDate").value = new Date(
                    event.finishDate
                  );
                  document.getElementById(
                    "registered"
                  ).value = event.createdBy.registered.join(",");
                  document.getElementById("tags").value = event.tags.join(",");
                  document.getElementById("place").value = event.place;
                  document.getElementById(
                    "resources"
                  ).value = event.resources.join(", ");
                  document.getElementById(
                    "description"
                  ).value = event.description;
                  document.getElementById("img").src = photo;
                  $(".ui.dropdown").dropdown();
                  $("#multi-select").dropdown();
                  $(".tags").dropdown({
                    allowAdditions: true
                  });

                  btnEvent.addEventListener("click", ev => {
                    ev.preventDefault();
                    const file = document.getElementById("photo").files[0];

                    if (file) {
                      const data = new FormData();
                      data.append("photo", file);
                      data.append("filename", filename.replace(/\s/g, ""));
                      utils.postToServer(
                        storageAddress,
                        "/img",
                        data,
                        (err, res) => {
                          if (err || !res.body.success) {
                            document.getElementById("msg").innerHTML = `
                            <div class="ui negative message center aligned container">
                              <i class="close icon"></i>
                              <div class="header">
                                Problemas guardando la imagen
                              </div>
                              <p>Intenta nuevamente</p>
                            </div>`;
                          } else {
                            const event = {
                              name: document.getElementById("name").value,
                              startDate: new Date(
                                document.getElementById("startDate").value
                              ),
                              finishDate: new Date(
                                document.getElementById("finishDate").value
                              ),
                              author: localStorage.username,
                              description: document.getElementById(
                                "description"
                              ).value,
                              createdBy: {
                                registered: document
                                  .getElementById("registered")
                                  .value.split(","),
                                others: []
                              },
                              img: res.body.data,
                              tags: document
                                .getElementById("tags")
                                .value.split(","),
                              place: document.getElementById("place").value,
                              resources: document
                                .getElementById("resources")
                                .value.split(",")
                            };

                            utils.putSecuredData(
                              backendAddress,
                              `${mountPoint}/${eventId}`,
                              event,
                              (err, res) => {
                                if (err || !res.body.success) {
                                  document.getElementById("msg").innerHTML = `
                                <div class="ui negative message center aligned container">
                                  <i class="close icon"></i>
                                  <div class="header">
                                    Problemas para crear evento
                                  </div>
                                  <p>Verifica que los datos sean los correctos e intenta nuevamente</p>
                                </div>`;
                                } else
                                  window.location.href = `/events/${res.body.data._id}`;
                              }
                            );
                          }
                        }
                      );
                    } else {
                      const event = {
                        name: document.getElementById("name").value,
                        startDate: new Date(
                          document.getElementById("startDate").value
                        ),
                        finishDate: new Date(
                          document.getElementById("finishDate").value
                        ),
                        author: localStorage.username,
                        description: document.getElementById(
                          "description"
                        ).value,
                        createdBy: {
                          registered: document
                            .getElementById("registered")
                            .value.split(","),
                          others: []
                        },
                        tags: document.getElementById("tags").value.split(","),
                        place: document.getElementById("place").value,
                        resources: document
                          .getElementById("resources")
                          .value.split(",")
                      };
                      utils.putSecuredData(
                        backendAddress,
                        `${mountPoint}/${eventId}`,
                        event,
                        (err, res) => {
                          if (err || !res.body.success) {
                            document.getElementById("msg").innerHTML = `
                            <div class="ui negative message center aligned container">
                              <i class="close icon"></i>
                              <div class="header">
                                Problemas para crear evento
                              </div>
                              <p>Verifica que los datos sean los correctos e intenta nuevamente</p>
                            </div>`;
                          } else
                            window.location.href = `/events/${res.body.data._id}`;
                        }
                      );
                    }
                  });
                } else {
                  window.location.href = "/";
                }
              }
            }
          );
        }
      });
    }
  });
};
