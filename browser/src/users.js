const utils = require("../utils");

module.exports = (page, mountPoint) => {
  page(mountPoint, ctx => {
    utils.getResourceFromServer(backendAddress, mountPoint, (err, res) => {
      if (err || !res.body.success) {
        document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para mostrar usuarios
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
      } else {
        let users = res.body.data;
        users = users.map(user => {
          let photo = "/img/user.png";
          let cv = "#";
          if (user.photo) photo = user.photo;
          if (user.cv) cv = user.cv;
          return `
              <div class="ui card">
                <div class="content">
                  <img class="right floated mini ui image modal-profile" src="${photo}" id="${user.username}">
                  <a class="header username" id="${user.username}" href="http://sirius.utp.edu.co/users/${user.username}">${user.firstName} ${user.lastName}</a>
                  <div class="meta">
                    <i>${user.professionalProfile.join(", ")}</i>
                  </div>
                  <div class="meta">
                    <b>${user.status.join(", ")}</b>
                  </div>
                  <div class="meta">
                    <a>${user.email}</a>
                  </div>
                </div>
                <div class="extra content">
                  <div class="ui two buttons">
                    <a class="ui button blue" target="_blank" href="#"> Web</a>
                    <a class="ui button teal" target="_blank" href="${cv}">CV</a>
                  </div>
                </div>
              </div>
              <div class="ui basic modal profile-${user.username}">
                <i class="close icon"></i>
                <div class="ui icon header">
                  <p>${user.firstName} ${user.lastName}</p>
                  <img class="medium ui image" src="${photo}">
                </div>
              </div>`;
        });

        users = users.join("\n");
        document.getElementById("members").innerHTML = users;
        const usernames = document.querySelectorAll(".username");
        for (let i = 0; i < usernames.length; i++) {
          usernames[i].addEventListener("click", ev => {
            ev.preventDefault();
            window.location.href = `/users/${ev.target.id}`;
          });
        }
      }
    });
  });

  page(`${mountPoint}/pending`, ctx => {
    if (utils.isAuthorized()) {
      utils.getSecuredResource(
        backendAddress,
        `${mountPoint}/auth?status=pending`,
        (err, res) => {
          if (err || !res.body.success) {
            document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Problemas para mostrar usuarios
              </div>
              <p>Intenta nuevamente</p>
            </div>`;
          } else {
            let users = res.body.data;
            users = users.map(user => {
              let photo = "/img/admin.jpg";
              if (user.photo) photo = user.photo;
              return `
              <div class="ui card">
                <div class="content">
                  <div class="header">${user.firstName} ${user.lastName}</div>
                </div>
                <div class="content">
                  <div class="ui small form open-sans">
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="name" placeholder="Usuario" value="${user.username}" readonly>
                      </div>
                    </div>
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="at icon"></i>
                        <input type="text" name="email" value="${user.email}" placeholder="Correo Eléctronico" readonly>
                      </div>
                    </div>
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="student icon"></i>
                        <input type="text" name="profession" value="${user.professionalProfile.join(", ")}" readonly placeholder="Titulo">
                      </div>
                    </div>
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="spy icon"></i>
                        <input type="text" name="position" value="${user.status.join(", ")}" readonly placeholder="Posicion">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="extra content">
                  <div class="ui two buttons">
                    <button class="ui button green accept" id="accept-${user.username}"><i class="checkmark icon"></i>Aprobar</button>
                    <button class="ui button red reject" id="reject-${user.username}"><i class="remove icon"></i>Rechazar</button>
                  </div>
                </div>
              </div>`;
            });
            users = users.join("\n");
            if (users.length > 0) {
              document.getElementById("pendingUsers").innerHTML = users;
              const accept = document.querySelectorAll(".accept");
              const reject = document.querySelectorAll(".reject");

              for (let i = 0; i < accept.length; i++) {
                accept[i].addEventListener("click", ev => {
                  const id = ev.target.id.split("-")[1];
                  const user = { privilegeType: "2" };
                  utils.putSecuredData(
                    backendAddress,
                    `${mountPoint}/${id}`,
                    user,
                    (err, res) => {
                      if (err || !res.body.success) {
                        document.getElementById("msg").innerHTML = `
                          <div class="ui negative message center aligned container">
                            <i class="close icon"></i>
                            <div class="header">
                              Problemas para aprobar cuenta
                            </div>
                            <p>Intenta nuevamente</p>
                          </div>`;
                      } else
                        window.location.href = "/users/pending";
                    }
                  );
                });
              }

              for (let i = 0; i < reject.length; i++) {
                reject[i].addEventListener("click", ev => {
                  const id = ev.target.id.split("-")[1];
                  const user = { privilegeType: "3" };
                  utils.putSecuredData(
                    backendAddress,
                    `${mountPoint}/${id}`,
                    user,
                    (err, res) => {
                      if (err || !res.body.success) {
                        document.getElementById("msg").innerHTML = `
                          <div class="ui negative message center aligned container">
                            <i class="close icon"></i>
                            <div class="header">
                              Problemas para rechazar cuenta
                            </div>
                            <p>Intenta nuevamente</p>
                          </div>`;
                      } else
                        window.location.href = "/users/pending";
                    }
                  );
                });
              }
            } else {
              window.location.href = "/";
            }
          }
        }
      );
    }
  });

  page(`${mountPoint}/admin`, ctx => {
    if (utils.isAuthorized() && window.localStorage.valid === "true") {
      utils.getSecuredResource(
        backendAddress,
        `${mountPoint}/auth`,
        (err, res) => {
          if (err || !res.body.success) {
            document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Problemas para mostrar usuarios
              </div>
              <p>Intenta nuevamente</p>
            </div>`;
          } else {
            let users = res.body.data;
            users = users.map(user => {
              if (user.username === localStorage.username) return "";
              let isAdmin = "";
              let photo = "/img/admin.jpg";
              if (user.photo) photo = user.photo;
              if (user.privilegeType === "1") {
                isAdmin = `
                  <div class="field row">
                    <div class="ui checkbox">
                      <input type="checkbox" class="enabled-${user._id}" disabled id="adm-${user.username}" checked>
                      <label>¿Es administrador?</label>
                    </div>
                  </div>`;
              } else if (user.privilegeType === "2") {
                isAdmin = `
                  <div class="field row">
                    <div class="ui checkbox">
                      <input type="checkbox" class="enabled-${user._id}" disabled id="adm-${user.username}">
                      <label>¿Es administrador?</label>
                    </div>
                  </div>`;
              }
              return `
              <div class="card">
                <div class="content">
                  <img class="right floated mini ui image modal-profile" src="${photo}" alt="" class="round" id="${user.username}" />
                  <div class="ui basic modal modal-${user.username}" >
                    <div class="ui center aligned"
                      <img src="${photo}" alt="" class="round"/>
                      <div class="ui header">
                        <img src="${photo}" alt="" class="round"/>
                      </div>
                    </div>
                  </div>
                  <div class="header">
                    ${user.firstName} ${user.lastName}
                  </div>
                  <form class="ui description form open-sans">
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="name" class="enabled-${user._id}" id="username-${user.username}" placeholder="Usuario" value="${user.username}" disabled>
                      </div>
                    </div>
                    <div class="field row">
                      <div class="ui left icon input">
                        <i class="at icon"></i>
                        <input type="text" name="email" class="enabled-${user._id}" id="email-${user.username}" placeholder="Correo Eléctronico" value="${user.email}" disabled>
                      </div>
                    </div>
                    <div class="field row">
                      <select name="rol" multiple="" class="ui fluid dropdown multi-select" id="rol-${user.username}">
                        <option value="">Rol</option>
                        <option value="Investigador(a)" ${user.status.includes("Investigador(a)") ? "selected" : ""}>Investigador(a)</option>
                        <option value="Monitor(a)" ${user.status.includes("Monitor(a)") ? "selected" : ""}>Monitor(a)</option>
                        <option value="Docente" ${user.status.includes("Docente") ? "selected" : ""}>Docente</option>
                        <option value="Director General" ${user.status.includes("Director General") ? "selected" : ""}>Director General</option>
                        <option value="Director Cientifico" ${user.status.includes("Director Cientifico") ? "selected" : ""}>Director Cientifico</option>
                      </select>
                    </div>
                    <div class="field row">
                      <select name="profesion" class="ui fluid dropdown multi-select" id="profesion-${user.username}" multiple="" >
                        <option value="">Profesión</option>
                        <option value="Estudiante" ${user.professionalProfile.includes("Estudiante") ? "selected" : ""}>Estudiante</option>
                        <option value="Ingeniero(a)" ${user.professionalProfile.includes("Ingeniero(a)") ? "selected" : ""}>Ingeniero(a)</option>
                        <option value="Magister" ${user.professionalProfile.includes("Magister") ? "selected" : ""}>Magister</option>
                        <option value="Doctor(a)" ${user.professionalProfile.includes("Doctor(a)") ? "selected" : ""}>Doctor(a)</option>
                        <option value="Especialista" ${user.professionalProfile.includes("Especialista") ? "selected" : ""}>Especialista</option>
                      </select>
                    </div>
                    ${isAdmin}
                    <div class="ui centered aligned grid" style="cursor: pointer;">
                      <div class="two wide column">
                        <i class="large edit grey icon editData" data-id="${user._id}" id="edit-${user.username}"></i>
                      </div>
                      <div class="two wide column" style="cursor: pointer; display: none" id="show-${user.username}">
                        <i class="large save grey icon saveData" id="save-${user.username}"></i>
                      </div>
                      <div class="twelve wide column">
                        <select name="state" id="state-${user.username}" class="ui fluid dropdown fixing-position">
                          <option value="">Estado</option>
                          <option value="2" ${user.privilegeType === "2" || user.privilegeType === "1" ? "selected" : ""}>Activo</option>
                          <option value="3" ${user.privilegeType === "3" ? "selected" : ""}>Inactivo</option>
                        </select>
                      </div>
                    </div>
                  <div class="ui error message"></div>
                  </form>
                </div>
              </div>`;
            });
            users = users.join("\n");
            if (users.length > 0) {
              document.getElementById("admin").innerHTML = users;
              $(".ui.dropdown").dropdown();
              const edit = document.querySelectorAll(".editData");
              const save = document.querySelectorAll(".saveData");

              for (let i = 0; i < edit.length; i++) {
                edit[i].addEventListener("click", ev => {
                  const username = ev.target.id.split("edit-")[1];
                  const userId = ev.target.getAttribute("data-id");
                  const enabled = document.querySelectorAll(
                    `.enabled-${userId}`
                  );
                  let flag = true;

                  for (let j = 0; j < enabled.length; j++) {
                    enabled[j].disabled = !enabled[j].disabled;
                    flag = enabled[j].disabled;
                  }
                  if (flag) {
                    document.getElementById(
                      `show-${username}`
                    ).style.display = "none";
                  } else {
                    document.getElementById(
                      `show-${username}`
                    ).style.display = "block";
                  }
                });
              }

              if (save) {
                for (let i = 0; i < save.length; i++) {
                  save[i].addEventListener("click", ev => {
                    const username = ev.target.id.split("save-")[1];
                    const usr = document.getElementById(
                      `username-${username}`
                    ).value;
                    const email = document.getElementById(
                      `email-${username}`
                    ).value;
                    const status = utils.getSelectValues(
                      document.getElementById(`rol-${username}`)
                    );
                    const professionalProfile = utils.getSelectValues(
                      document.getElementById(`profesion-${username}`)
                    );
                    let privilegeType = "3";
                    if (document.getElementById(`adm-${username}`)) {
                      const isAdmin = document.getElementById(
                        `adm-${username}`
                      );
                      if (isAdmin.checked) {
                        privilegeType = "1";
                      } else {
                        privilegeType = "2";
                      }
                    }

                    if (
                      privilegeType === "3" ||
                      document.getElementById(`state-${username}`).value === "3"
                    ) {
                      privilegeType = document.getElementById(
                        `state-${username}`
                      ).value;
                    }

                    const user = {
                      username: usr,
                      email,
                      status,
                      professionalProfile,
                      privilegeType
                    };

                    utils.putSecuredData(
                      backendAddress,
                      `/users/${username}`,
                      user,
                      (err, res) => {
                        if (err || !res.body.success) {
                          document.getElementById("msg").innerHTML = `
                          <div class="ui negative message center aligned container">
                            <i class="close icon"></i>
                            <div class="header">
                              Problemas para actualizar los datos del usuario ${usr}
                            </div>
                            <p>Intenta nuevamente</p>
                          </div>`;
                        } else {
                          window.location.href = "/users/admin";
                          document.getElementById("msg").innerHTML = `
                            <div class="ui positive message center aligned container">
                              <i class="close icon"></i>
                              <div class="header">
                                Los datos de ${res.body.data.username} han sido actualizados
                              </div>
                            </div>`;
                        }
                      }
                    );
                  });
                }
              }
            } else {
              window.location.href = "/";
            }
          }
        }
      );
    }
  });

  page(`${mountPoint}/register`, ctx => {
    const btnCreate = document.getElementById("btn_create");
    btnCreate.addEventListener("click", ev => {
      ev.preventDefault();
      let warnings = [];
      if (!document.getElementById("firstName").value)
        warnings.push("<li>* Nombre(s)</li>");
      if (!document.getElementById("lastName").value)
        warnings.push("<li>* Apellido(s)</li>");
      if (!document.getElementById("username").value)
        warnings.push("<li>* Nombre de usuario</li>");
      if (!document.getElementById("email").value)
        warnings.push("<li>* Correo</li>");
      if (!document.getElementById("password").value)
        warnings.push("<li>* Contraseña</li>");
      if (!document.getElementById("cpassword").value)
        warnings.push("<li>* Confirmar Contraseña</li>");
      if (!document.getElementById("profesion").value)
        warnings.push("<li>* Profesión</li>");
      if (!document.getElementById("rol").value)
        warnings.push("<li>* Rol</li>");
      if (warnings.length > 0) {
        document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Los siguientes campos son requeridos:
            </div>
            <ul>${warnings.join("")}</ul>
          </div>`;
      } else {
        if (
          document.getElementById("cpassword").value ===
          document.getElementById("password").value
        ) {
          const user = {
            firstName: document.getElementById("firstName").value,
            lastName: document.getElementById("lastName").value,
            username: document.getElementById("username").value,
            email: document.getElementById("email").value,
            password: document.getElementById("password").value,
            professionalProfile: utils.getSelectValues(
              document.getElementById("profesion")
            ),
            status: utils.getSelectValues(document.getElementById("rol"))
          };

          utils.postToServer(backendAddress, mountPoint, user, (err, res) => {
            if (err || !res.body.success) {
              document.getElementById("msg").innerHTML = `
                <div class="ui negative message center aligned container">
                  <i class="close icon"></i>
                  <div class="header">
                    Problemas para registrar usuario
                  </div>
                  <p>Verifica que los datos sean los correctos e intenta nuevamente</p>
                </div>`;
            } else
              page("/");
          });
        } else {
          document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Problemas para registrar usuario
              </div>
              <p>Las contraseñas no coinciden</p>
            </div>`;
        }
      }
    });
  });

  page(`${mountPoint}/profile`, ctx => {
    if (utils.isAuthorized()) {
      let user, form, img, cv;
      const resource = `${mountPoint}/${localStorage.username}`;
      const btnEdit = document.getElementById("btn_edit");

      utils.getResourceFromServer(backendAddress, resource, (err, res) => {
        if (err || !res.body.success) {
          document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para obtener los datos del usuario ${localStorage.username}
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
        } else {
          user = res.body.data;
          document.title = user.firstName + " " + user.lastName;
          document.getElementById("username").innerText = user.username;
          form = document.getElementById("editForm");
          img = document.getElementById("imgProfile");
          cv = document.getElementById("cv");
          const inputs = form.elements;

          form.firstName.value = user.firstName;
          form.lastName.value = user.lastName;
          form.email.value = user.email;
          utils.setSelectValues(form.profesion, user.professionalProfile);
          utils.setSelectValues(form.rol, user.status);

          if (user.photo) img.src = user.photo;
          if (user.cv) {
            cv.style.display = "block";
            cv.href = user.cv;
          }

          for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].id === "doc") continue;
            inputs[i].disabled = true;
          }

          const edit = document.getElementById("edit");
          edit.addEventListener("click", ev => {
            let flag = true;
            for (let i = 0; i < inputs.length; i++) {
              if (inputs[i].id === "doc") continue;
              inputs[i].disabled = !inputs[i].disabled;
              flag = inputs[i].disabled;
            }
            if (!flag) {
              btnEdit.style.display = "block";
            } else {
              btnEdit.style.display = "none";
            }
          });
        }
      });

      btnEdit.addEventListener("click", ev => {
        ev.preventDefault();
        const file = document.getElementById("photo").files[0];
        const doc = document.getElementById("doc").files[0];

        if (file && doc) {
          const data = new FormData();
          data.append("doc", doc);
          data.append("filename", user.username.replace(/\s/g, ""));
          utils.postToServer(storageAddress, "/docs", data, (err, res) => {
            if (err || !res.body.success) {
              document.getElementById("msg").innerHTML = `
              <div class="ui negative message center aligned container">
                <i class="close icon"></i>
                <div class="header">
                  Problemas guardando el documento
                </div>
                <p>Intenta nuevamente</p>
              </div>`;
            } else {
              const cvUrl = res.body.data;
              const data = new FormData();
              data.append("photo", file);
              data.append("filename", user.username.replace(/\s/g, ""));
              utils.postToServer(storageAddress, "/img", data, (err, res) => {
                if (err || !res.body.success) {
                  document.getElementById("msg").innerHTML = `
                  <div class="ui negative message center aligned container">
                    <i class="close icon"></i>
                    <div class="header">
                      Problemas guardando la imagen
                    </div>
                    <p>Intenta nuevamente</p>
                  </div>`;
                } else {
                  const user = {
                    firstName: form.firstName.value,
                    lastName: form.lastName.value,
                    email: form.email.value,
                    professionalProfile: utils.getSelectValues(
                      document.getElementById("profesion")
                    ),
                    status: utils.getSelectValues(
                      document.getElementById("rol")
                    ),
                    photo: res.body.data,
                    cv: cvUrl
                  };

                  utils.putSecuredData(
                    backendAddress,
                    resource,
                    user,
                    (err, res) => {
                      if (err || !res.body.success) {
                        document.getElementById("msg").innerHTML = `
                        <div class="ui negative message center aligned container">
                          <i class="close icon"></i>
                          <div class="header">
                            Problemas para actualizar los datos del usuario ${localStorage.username}
                          </div>
                          <p>Intenta nuevamente</p>
                        </div>`;
                      } else {
                        localStorage.username = res.body.data.username;
                        document.getElementById("textPhoto").value = "";
                        document.getElementById("textCv").value = "";
                        window.location.href = "/users/profile";
                      }
                    }
                  );
                }
              });
            }
          });
        } else if (doc) {
          const data = new FormData();
          data.append("doc", doc);
          data.append("filename", user.username.replace(/\s/g, ""));
          utils.postToServer(storageAddress, "/docs", data, (err, res) => {
            if (err || !res.body.success) {
              document.getElementById("msg").innerHTML = `
              <div class="ui negative message center aligned container">
                <i class="close icon"></i>
                <div class="header">
                  Problemas guardando el documento
                </div>
                <p>Intenta nuevamente</p>
              </div>`;
            } else {
              const user = {
                firstName: form.firstName.value,
                lastName: form.lastName.value,
                email: form.email.value,
                professionalProfile: utils.getSelectValues(
                  document.getElementById("profesion")
                ),
                status: utils.getSelectValues(document.getElementById("rol")),
                cv: res.body.data
              };

              utils.putSecuredData(
                backendAddress,
                resource,
                user,
                (err, res) => {
                  if (err || !res.body.success) {
                    document.getElementById("msg").innerHTML = `
                  <div class="ui negative message center aligned container">
                    <i class="close icon"></i>
                    <div class="header">
                      Problemas para actualizar los datos del usuario ${localStorage.username}
                    </div>
                    <p>Intenta nuevamente</p>
                  </div>`;
                  } else {
                    localStorage.username = res.body.data.username;
                    document.getElementById("textCv").value = "";
                    window.location.href = "/users/profile";
                  }
                }
              );
            }
          });
        } else if (file) {
          const data = new FormData();
          data.append("photo", file);
          data.append("filename", user.username.replace(/\s/g, ""));
          utils.postToServer(storageAddress, "/img", data, (err, res) => {
            if (err || !res.body.success) {
              document.getElementById("msg").innerHTML = `
              <div class="ui negative message center aligned container">
                <i class="close icon"></i>
                <div class="header">
                  Problemas guardando la imagen
                </div>
                <p>Intenta nuevamente</p>
              </div>`;
            } else {
              const user = {
                firstName: form.firstName.value,
                lastName: form.lastName.value,
                email: form.email.value,
                professionalProfile: utils.getSelectValues(
                  document.getElementById("profesion")
                ),
                status: utils.getSelectValues(document.getElementById("rol")),
                photo: res.body.data
              };

              utils.putSecuredData(
                backendAddress,
                resource,
                user,
                (err, res) => {
                  if (err || !res.body.success) {
                    document.getElementById("msg").innerHTML = `
                  <div class="ui negative message center aligned container">
                    <i class="close icon"></i>
                    <div class="header">
                      Problemas para actualizar los datos del usuario ${localStorage.username}
                    </div>
                    <p>Intenta nuevamente</p>
                  </div>`;
                  } else {
                    localStorage.username = res.body.data.username;
                    document.getElementById("textPhoto").value = "";
                    window.location.href = "/users/profile";
                  }
                }
              );
            }
          });
        } else {
          const user = {
            firstName: form.firstName.value,
            lastName: form.lastName.value,
            email: form.email.value,
            professionalProfile: utils.getSelectValues(
              document.getElementById("profesion")
            ),
            status: utils.getSelectValues(document.getElementById("rol"))
          };

          utils.putSecuredData(backendAddress, resource, user, (err, res) => {
            if (err || !res.body.success) {
              document.getElementById("msg").innerHTML = `
              <div class="ui negative message center aligned container">
                <i class="close icon"></i>
                <div class="header">
                  Problemas para actualizar los datos del usuario ${localStorage.username}
                </div>
                <p>Intenta nuevamente</p>
              </div>`;
            } else {
              localStorage.username = res.body.data.username;
              document.getElementById("textPhoto").value = "";
              window.location.href = "/users/profile";
            }
          });
        }
      });
    }
  });

  page(`${mountPoint}/:username`, ctx => {
    let user, form, img;
    const resource = `${mountPoint}/${ctx.params.username}`;

    utils.getResourceFromServer(backendAddress, resource, (err, res) => {
      if (err || !res.body.success) {
        document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Problemas para obtener los datos del usuario ${ctx.params.username}
            </div>
            <p>Intenta nuevamente</p>
          </div>`;
      } else {
        user = res.body.data;
        document.title = user.firstName + " " + user.lastName;
        const fullname = document.getElementById("fullname");
        form = document.getElementById("editForm");
        img = document.getElementById("imgProfile");
        const inputs = form.elements;

        fullname.innerText = user.firstName + " " + user.lastName;
        form.username.value = user.username;
        form.email.value = user.email;
        form.profession.value = user.professionalProfile.join(", ");
        form.position.value = user.status.join(", ");
        if (user.photo) img.src = user.photo;
      }
    });
  });

  page(`${mountPoint}/emailRecovery`, ctx => {
    if (!localStorage.username) {
      const sendEmail = document.getElementById("btn_send_email");
      sendEmail.addEventListener("click", ev => {
        ev.preventDefault();
        let warning = "";
        if (!document.getElementById("email").value)
          warning = "<p>* Correo </p>";
        if (warning) {
          document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Los siguientes campos son requeridos:
              </div>
              ${warning}
            </div>`;
        } else {
          const content = { email: document.getElementById("email").value };
          utils.postToServer(
            backendAddress,
            `${mountPoint}/emailRecovery`,
            content,
            (err, res) => {
              if (err || !res.body.success) {
                document.getElementById("msg").innerHTML = `
                  <div class="ui negative message center aligned container">
                    <i class="close icon"></i>
                    <div class="header">
                      Problemas para recuperar contraseña
                    </div>
                    <p>Verifica que el correo sea el correcto</p>
                  </div>`;
              } else {
                window.localStorage.recovery = true;
                page("/");
              }
            }
          );
        }
      });
    } else {
      window.location.href = "/";
    }
  });

  page(`${mountPoint}/reset/:token`, ctx => {
    if (!localStorage.username) {
      const confirm = document.getElementById("btn_confirm");
      confirm.addEventListener("click", ev => {
        ev.preventDefault();
        let warnings = [];
        if (!document.getElementById("password").value)
          warnings.push("<li>* Contraseña</li>");
        if (!document.getElementById("cpassword").value)
          warnings.push("<li>* Confirmar Contraseña</li>");
        if (warnings.length > 0) {
          document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Los siguientes campos son requeridos:
              </div>
              <ul>${warnings.join("")}</ul>
            </div>`;
        } else {
          const content = {
            password: document.getElementById("password").value,
            cpassword: document.getElementById("cpassword").value
          };
          if (content.password === content.cpassword) {
            utils.putToServer(
              backendAddress,
              `${mountPoint}/reset/${ctx.params.token}`,
              content,
              (err, res) => {
                if (err || !res.body.success) {
                  document.getElementById("msg").innerHTML = `
                    <div class="ui negative message center aligned container">
                      <i class="close icon"></i>
                      <div class="header">
                        Problemas establecer una nueva contraseña
                      </div>
                      <p>Verifica que no sea la contraseña que tenias anteriormente e intenta nuevamente</p>
                    </div>`;
                } else {
                  window.location.href = "/session/login";
                }
              }
            );
          } else {
            document.getElementById("msg").innerHTML = `
            <div class="ui negative message center aligned container">
              <i class="close icon"></i>
              <div class="header">
                Las contraseñas no coinciden
              </div>
              <p>Verifica que las contraseñas sean iguales </p>
            </div>`;
          }
        }
      });
    } else {
      window.location.href = "/";
    }
  });
};
