const utils = require("../utils");

module.exports = (page, mountPoint) => {
  page(`${mountPoint}/login`, ctx => {
    if (!localStorage.username) {
      const href = document.getElementById("redirectRegister");
      href.addEventListener("click", ev => {
        ev.preventDefault();
        window.location.href = "/users/register";
      });

      const recovery = document.getElementById("redirectRecovery");
      recovery.addEventListener("click", ev => {
        ev.preventDefault();
        window.location.href = "/users/emailRecovery";
      });

      const btnLogin = document.getElementById("btn_login");
      btnLogin.addEventListener("click", ev => {
        ev.preventDefault();
        let warnings = [];
        if (!document.getElementById("username").value)
          warnings.push("<li>* Nombre de usuario</li>");
        if (!document.getElementById("password").value)
          warnings.push("<li>* Contraseña</li>");
        if (warnings.length > 0) {
          document.getElementById("msg").innerHTML = `
          <div class="ui negative message center aligned container">
            <i class="close icon"></i>
            <div class="header">
              Los siguientes campos son requeridos:
            </div>
            <ul>${warnings.join("")}</ul>
          </div>`;
        } else {
          const user = {
            username: document.getElementById("username").value,
            password: document.getElementById("password").value
          };

          utils.postToServer(
            backendAddress,
            `${mountPoint}/login`,
            user,
            (err, res) => {
              if (err || !res.body.success) {
                document.getElementById("msg").innerHTML = `
                <div class="ui negative message center aligned container">
                  <i class="close icon"></i>
                  <div class="header">
                    Problemas para iniciar sesión
                  </div>
                  <p>Verifica que el campo de usuario y contraseña sean los correctos</p>
                </div>`;
              } else {
                window.localStorage.token = res.body.token;
                window.localStorage.username = res.body.data;
                window.localStorage.valid = res.body.valid;
                window.localStorage.isLogin = true;
                page("/");
              }
            }
          );
        }
      });
    } else {
      window.location.href = "/";
    }
  });
};
