const request = require("superagent");

module.exports = {
  isAuthorized() {
    if (!localStorage.username) {
      window.location.href = "/";
      return false;
    }
    return true;
  },

  getSelectValues(select) {
    let result = [];
    const options = select && select.options;

    for (let i = 0; i < options.length; i++) {
      const opt = options[i];

      if (opt.selected) {
        result.push(opt.value);
      }
    }
    return result;
  },

  setSelectValues(select, opts) {
    const options = select && select.options;

    for (let i = 0; i < options.length; i++) {
      const opt = options[i];
      opt.selected = opts.includes(opt.value);
    }
    $("#multi-select").dropdown();
    $(".multi-select").dropdown();
  },

  // Get all data of a resource from the server
  getResourceFromServer(url, resource, callback) {
    request.get(url + resource).set("Accept", "application/json").end(callback);
  },

  // Post something to the server
  postToServer(url, resource, data, callback) {
    request
      .post(url + resource)
      .send(data)
      .set("Accept", "application/json")
      .end(callback);
  },

  // Update something to the server
  putToServer(url, resource, data, callback) {
    request
      .put(url + resource)
      .send(data)
      .set("Accept", "application/json")
      .end(callback);
  },

  // Delete a resource from the server
  deleteResourceFromServer(url, resource, callback) {
    request.del(url + resource).end(callback);
  },

  // Requests with JWT
  // Get all data of a resource from the server
  getSecuredResource(url, resource, callback) {
    request
      .get(url + resource)
      .set("Accept", "application/json")
      .set("Authorization", localStorage.token)
      .end(callback);
  },

  // Post something to the server
  postSecuredData(url, resource, data, callback) {
    request
      .post(url + resource)
      .set("Authorization", localStorage.token)
      .set("Accept", "application/json")
      .send(data)
      .end(callback);
  },

  // Update something to the server
  putSecuredData(url, resource, data, callback) {
    request
      .put(url + resource)
      .set("Authorization", localStorage.token)
      .set("Accept", "application/json")
      .send(data)
      .end(callback);
  },

  // Delete a resource from the server
  delSecuredResource(url, resource, callback) {
    request
      .del(url + resource)
      .set("Authorization", localStorage.token)
      .end(callback);
  }
};
