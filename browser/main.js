const page = require("page");
const jwtDecode = require("jwt-decode");

const env = process.env.NODE_ENV || "dev";
const config = require("../config/" + env);
window.backendAddress = config.backend.url;
window.storageAddress = config.storage.url;

const buttons = document.getElementById("sticky-session");
if (window.localStorage.token && window.localStorage.username) {
  if (Date.now() > jwtDecode(localStorage.token).exp) {
    delete localStorage.token;
    delete localStorage.username;
    delete localStorage.isLogin;
    delete localStorage.valid;
    window.location.href = "/";
  }
  let pending = "";
  if (window.localStorage.valid === "true") {
    pending = `
      <li>
        <a class="pending" id="pending">
          <i class="fa fa-users" aria-hidden="true"></i>
          <span>Aprobar Cuentas</span>
        </a>
      </li>
      <li>
        <a class="admin" id="adminUsers">
          <i class="fa fa-cogs" aria-hidden="true"></i>
          <span>Admin</span>
        </a>
      </li>`;
  }
  const content = `
  <ul>
    <li>
      <a id="userProfile" class="user">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>${window.localStorage.username}</span>
      </a>
    </li>
    ${pending}
    <li>
      <a class="add" id="addUser">
        <i class="fa fa-user-plus" aria-hidden="true"></i>
        <span>Crear Usuario</span>
      </a>
    </li>
    <li>
      <a class="event" id="eventCreate">
        <i class="fa fa-calendar" aria-hidden="true"></i>
        <span>Crear Evento</span>
      </a>
    </li>
    <li>
      <a class="exit" id="logout">
        <i class="fa fa-sign-out" aria-hidden="true"></i>
        <span>Salir</span>
      </a>
    </li>
  </ul>`;

  buttons.innerHTML = content;
  const logout = document.getElementById("logout");
  logout.addEventListener("click", ev => {
    ev.preventDefault();
    delete localStorage.token;
    delete localStorage.username;
    delete localStorage.isLogin;
    window.location.href = "/";
  });

  const profile = document.getElementById("userProfile");
  profile.addEventListener("click", ev => {
    ev.preventDefault();
    window.location.href = "/users/profile";
  });

   const add = document.getElementById("addUser");
  add.addEventListener("click", ev => {
    ev.preventDefault();
    window.location.href = "/users/register";
  });

  const event = document.getElementById("eventCreate");
  event.addEventListener("click", ev => {
    ev.preventDefault();
    window.location.href = "/events/create";
  });

  if (window.localStorage.valid === "true") {
    const pending = document.getElementById("pending");
    const admin = document.getElementById("adminUsers");

    pending.addEventListener("click", ev => {
      ev.preventDefault();
      window.location.href = "/users/pending";
    });

    admin.addEventListener("click", ev => {
      ev.preventDefault();
      window.location.href = "/users/admin";
    });
  }
}

// Routes for the API
const users = require("./src/users");
const session = require("./src/session");
const projects = require("./src/projects");
const events = require("./src/events");

users(page, "/users");
session(page, "/session");
projects(page, "/projects");
events(page, "/events");

page({
  dispatch: true
});
